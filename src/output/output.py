"""
TicTacGo: Output-Client.

This client connects to the server, receives data, interprets the data and
uses OLA to connect to the LEDs
"""

# Import array functions
import array

# Import socketIO stuff
try:
    from socketIO_client import SocketIO, BaseNamespace
except ImportError:
    raise RuntimeError("Need socketIO to connect to server.")

# Import OLA stuff
try:
    from ola.ClientWrapper import ClientWrapper
except ImportError:
    raise RuntimeError("Need OLA to start ClientWrapper.")

# Set dmx universe used in OLA
universe = 1

# Create array for all needed dmx channels and init every channel with zero
dmx = array.array('B')
for i in range(0, 75):
    dmx.append(0)

# Initialize ClientWrapper
wrapper = ClientWrapper()
client = wrapper.Client()


# Define DmxSent
def DmxSent(state):
    """State for the SendDmx-function."""
    wrapper.Stop()


class TtgNamespace(BaseNamespace):
    """
    SocketIO-Namespace-Class.

    Handles Connection in the defined namespace.
    """

    @classmethod
    def on_update_led(cls, *args):
        """On received update-led-emit, this function starts."""
        # Get control message (fieldID + fieldColor) from server (args)
        data = args[0]

        # Insert field-color values into one long array of all needed
        # dmx channels
        for k in range(0, 6):
            # every 7th channel a new dmx device begins
            dmx[(data['id']*7)+k] = data['color'][k]

        # Send dmx values to OLA
        client.SendDmx(universe, dmx, DmxSent)
        wrapper.Run()

    @classmethod
    def on_initialize(cls, *args):
        """On received initialize-emit, print connected-status."""
        print("Connected.")

# Create SocketIO Client to listen to servers control messages
socketIO = SocketIO("https://mobile.mt.haw-hamburg.de", 3017)
ttg = socketIO.define(TtgNamespace, "/tictacgo/client")
ttg.on("update-led", ttg.on_update_led)
ttg.on("initialize", ttg.on_initialize)
socketIO.wait()
