/*
The Tic, Tac, Go! - App is based on the Evothings demo app "cordova-ibeacon":
https://github.com/mikaelkindborg/evo-demos/tree/master/Demos2015/cordova-ibeacon
*/

var app = (function() {
	// Application object
	var app = {};

	// Nearest ranged beacon
	var mNearestBeacon = null;

	// Timer that displays nearby beacons
	var mNearestBeaconEmitTimer = null;

	// Background flag
	var mAppInBackground = false;

	// Background notification id counter
	var mNotificationId = 0;
  
    // Here monitored beacon regions are defined
    // Uuids consists of '0' only to make input for developer easier
    // Major and Minor are set to field number
	var mRegions =
	[
		{
          id: 'field1',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 1,
          minor: 1
		},
		{
          id: 'field2',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 2,
          minor: 2
		},
        {
          id: 'field3',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 3,
          minor: 3
        },
        {
          id: 'field4',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 4,
          minor: 4
        },
        {
          id: 'field5',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 5,
          minor: 5
        },
        {
          id: 'field6',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 6,
          minor: 6
        },
        {
          id: 'field7',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 7,
          minor: 7
        },
        {
          id: 'field8',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 8,
          minor: 8
        },
        {
          id: 'field9',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 9,
          minor: 9
        },
        {
          id: 'regionJoin',
          uuid: '00000000-0000-0000-0000-000000000000',
          major: 0,
          minor: 0
        }
	];

	var socket;    
    const SERVER = "https://mobile.mt.haw-hamburg.de";    
    const NAMESPACE = "/tictacgo/client";
  

	app.initialize = function()	{
		document.addEventListener('deviceready', onDeviceReady, false);
		document.addEventListener('pause', onAppToBackground, false);
		document.addEventListener('resume', onAppToForeground, false);
	};

	function onDeviceReady() {
		// Start Evothings beacon tracking to always get the nearest beacon
        startMonitoringAndRanging();
        
        // Connect to server when nearest beacon is the "start area" beacon
        if(getBeaconId(mNearestBeacon) === "00000000-0000-0000-0000-000000000000:0:0") {
          socket = io.connect(SERVER + NAMESPACE);
          $("#event-message").css("visibility", "hidden"); // Hide message with start instructions
          $("#start-page").css("visibility", "visible"); // Show join page
        }            
        
        // Initialize join page with server-side stored team colors and disable full teams
        socket.on("initialize", function(data) {
          $("#team-0-div").css("background-color", data.colors[0]);
          $("#team-1-div").css("background-color", data.colors[1]);
          $("#team-0").prop("disabled", data.fullteam[0]);
          $("#team-1").prop("disabled", data.fullteam[1]);
          registerJoinFormListener();
        }); 
      
        // Display servers join message on successful join
        socket.on("joined", function(message) {
          $("#start-page").css("visibility", "hidden");
          $("#event-message > p").text(message);
          $("#event-message").css("visibility", "visible");
        });
        
        socket.on("update-field-status", function(fieldStatus) {
          displayFieldStatus(fieldStatus);
        });
        
        // React to game events emitted from server occuring once in the game
        socket.on("update-game-event", function(gameEvent) {
          if (gameEvent.id === "ready-check") { // Render a button to check if player is ready
            $("#event-message").css("visibility", "hidden");
            $("#ready-check").css("visibility", "visible");
            $("#btn-ready").click(function () {
              socket.emit("ready");
              $("#btn-ready").css("visibility", "hidden");
              $("#ready-check > h3").text("Waiting for other players...");
            });
          } 
          else if (gameEvent.id === "game-start") { // Render game page with field display
            $("#ready-check").css("visibility", "hidden");
            $("#game-page").css("visibility", "visible");
		    startNearestBeaconEmitTimer();
          } 
          else if (gameEvent.id === "game-countdown") { // Show a synced count-down before game starts
            $("#ready-check > h3").text("Game starts in: " + Math.floor(gameEvent.msg) + "...");
          } 
          else if (gameEvent.id === "game-over") {
            $("#game-page").css("visibility", "hidden");
            $("#event-message > h3").text("Game over!"); 
            $("#event-message > p").text(gameEvent.msg);            
            $("#event-message").css("visibility", "visible");
            stopNearestBeaconEmitTimer();
          }
        });
	}

	function onAppToBackground() {
		mAppInBackground = true;
	}

	function onAppToForeground() {
		mAppInBackground = false;
	}

	// Start emitting nearest beacon
    function startNearestBeaconEmitTimer() {
		mNearestBeaconEmitTimer = setInterval(emitNearestBeacon, 1000);
	}

	// Stop emitting nearest beacon
    function stopNearestBeaconEmitTimer() {
		clearInterval(mNearestBeaconEmitTimer);
		mNearestBeaconEmitTimer = null;
	}

	function startMonitoringAndRanging() {
		function onDidRangeBeaconsInRegion(result) {
			updateNearestBeacon(result.beacons);
		}

		function onError(errorMessage) {
			console.log('Monitoring beacons did fail: ' + errorMessage);
		}

		// Request permission from user to access location info
		cordova.plugins.locationManager.requestAlwaysAuthorization();

		// Create delegate object that holds beacon callback functions
		var delegate = new cordova.plugins.locationManager.Delegate();
		cordova.plugins.locationManager.setDelegate(delegate);

		// Set delegate function
		delegate.didRangeBeaconsInRegion = onDidRangeBeaconsInRegion;

		// Start monitoring and ranging beacons
		startMonitoringAndRangingRegions(mRegions, onError);
	}

	function startMonitoringAndRangingRegions(regions, errorCallback) {
		// Start monitoring and ranging regions
		for (var i in regions) {
			startMonitoringAndRangingRegion(regions[i], errorCallback);
		}
	}

	function startMonitoringAndRangingRegion(region, errorCallback) {
		// Create a region object
		var beaconRegion = new cordova.plugins.locationManager.BeaconRegion
        (
			region.id,
			region.uuid,
			region.major,
			region.minor
        );

		// Start ranging
		cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
			.fail(errorCallback)
			.done();

		// Start monitoring
		cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion)
			.fail(errorCallback)
			.done();
	}

	function getBeaconId(beacon) {
		return beacon.uuid + ':' + beacon.major + ':' + beacon.minor;
	}

	function isSameBeacon(beacon1, beacon2)	{
		return getBeaconId(beacon1) === getBeaconId(beacon2);
	}

	function isNearerThan(beacon1, beacon2)	{
		return beacon1.accuracy > 0
			&& beacon2.accuracy > 0
			&& beacon1.accuracy < beacon2.accuracy;
	}

	function updateNearestBeacon(beacons) {
		for (var i = 0; i < beacons.length; ++i) {
			var beacon = beacons[i];
			if (!mNearestBeacon) {
				mNearestBeacon = beacon;
                console.log(getBeaconId(mNearestBeacon));
			}
			else {
				if (isSameBeacon(beacon, mNearestBeacon)) {
                  mNearestBeacon = beacon;
				} else if (isNearerThan(beacon, mNearestBeacon)) {
                  mNearestBeacon = beacon;
                }
			}
		}      
	}

    // Emit nearest beacon to get corresponding field data from server
	function emitNearestBeacon() {
		if (mNearestBeacon) {
          socket.emit("update-nearest-beacon", getBeaconId(mNearestBeacon));
        }
	}
  
    // Parse and emit form values when player hits submit
    function registerJoinFormListener() {
      $("#join-form").submit(function(event) {
          var playerName = $("input[name='playername']").val();
          var playerTeamString = $("input[name='radio-playerteam']:checked").val();
          var playerTeamInt = parseInt(playerTeamString);
          socket.emit("join", {name: playerName, team: playerTeamInt});          
          event.preventDefault(); 
        }); 
    }
	
    // Display name, owner and status of current field
    function displayFieldStatus(data) {
      $("#current-field").text(data.fieldName);
      if(data.progress > 0)  {
        $("#field-progress").css("background-color", data.fieldColor);
        $("#field-progress").css("width", data.progress + "%");
      }
    }

	return app;

})();

app.initialize();
