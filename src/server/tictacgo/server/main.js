/*
  SERVER MODULE: TIC TAC GO

  This is the server-module for our TicTacGo-game, it uses the framework
  provided to get loaded and running.

  It provides a server for the mobile-clients to connect, and handles all the
  gamelogic.

  Authors: Finn Schröder, Felix Sjöö

*/

module.exports = {

  // Function called by framework
  initialize(socket, express, app) {

    // CONSTANTS
    // Team-colors used by the mobile clients
    const TEAM_COLORS = ["#FF0000", "#0000FF", "#CCCCCC"];
    // Team-colors as used by the output-client
    const LED_COLORS = [[255, 255, 0, 0, 0, 0, 0], [255, 0, 0, 255, 0, 0, 0], [255, 255, 255, 255, 0, 0, 0]];
    // Team-size
    const TEAM_SIZE = 5;
    // Countdown-Length after ready-check in s
    const GAME_COUNTDOWN = 5;
    // Game-update interval in ms
    const GAME_INTERVAL = 100;
    // Time needed to conquer a field with a one-player-advantage
    const GAME_CONQUERTIME = 10;

    // List linking beacon id to their physical position
    const BEACON_LIST = {
      "00000000-0000-0000-0000-000000000000:a:a": {x: -1, y: -1},
      "00000000-0000-0000-0000-000000000000:1:1": {x: 0, y: 0},
      "00000000-0000-0000-0000-000000000000:2:2": {x: 0, y: 1},
      "00000000-0000-0000-0000-000000000000:3:3": {x: 0, y: 2},
      "00000000-0000-0000-0000-000000000000:4:4": {x: 1, y: 0},
      "00000000-0000-0000-0000-000000000000:5:5": {x: 1, y: 1},
      "00000000-0000-0000-0000-000000000000:6:6": {x: 1, y: 2},
      "00000000-0000-0000-0000-000000000000:7:7": {x: 2, y: 0},
      "00000000-0000-0000-0000-000000000000:8:8": {x: 2, y: 1},
      "00000000-0000-0000-0000-000000000000:9:9": {x: 2, y: 2}
    };

    // List linking beacon id to their ingame-name
    const BEACON_NAMES = {
      "00000000-0000-0000-0000-000000000000:a:a": "START",
      "00000000-0000-0000-0000-000000000000:1:1": "FIELD 1",
      "00000000-0000-0000-0000-000000000000:2:2": "FIELD 2",
      "00000000-0000-0000-0000-000000000000:3:3": "FIELD 3",
      "00000000-0000-0000-0000-000000000000:4:4": "FIELD 4",
      "00000000-0000-0000-0000-000000000000:5:5": "FIELD 5",
      "00000000-0000-0000-0000-000000000000:6:6": "FIELD 6",
      "00000000-0000-0000-0000-000000000000:7:7": "FIELD 7",
      "00000000-0000-0000-0000-000000000000:8:8": "FIELD 8",
      "00000000-0000-0000-0000-000000000000:9:9": "FIELD 9"
    };

    // Field ids to adress LEDs
    const FIELD_NUM = [[0,1,2], [3,4,5], [6,7,8]];

    // VARIABLES
    // gamestatus: < 0: no game, < 0: countdown, 0: game running
    var gamestatus = -1;
    // iswinner saves who has won to recognize draws
    var iswinner = [false, false];
    // Array for saving players
    var players = [[], []];
    // Counting ready players
    var playersready = 0;
    // Field statuses (owner, contester, progress...)
    var beacon_status = [[],[],[]];


    // EXPRESS CONFIGURATIONS
    // TODO: Links (admin)
    app.use("/tictacgo", express.static(__dirname + "/../public"));
    app.use("/tictacgo/admin", express.static(__dirname + "/../public"));
    app.use("/tictacgo/tictacgo.apk", express.static(__dirname + "/../public/tictacgo.apk"));


    // SOCKET COMMUNICATIONS
    // Declaring namespace
    var ttg = socket.of("/tictacgo/client");

    // Connection handling
    ttg.on("connection", function(socket) {

      // Initialize, sending colors and if teams are full
      socket.emit("initialize", {
        colors: TEAM_COLORS,
        fullteam: [
          players[0].length >= TEAM_SIZE,
          players[1].length >= TEAM_SIZE
        ]
      });

      // On client join request
      socket.on("join", function(data) {

        // Variable for storing join-status
        var isjoined = false;

        // Check if team is valid and is not full
        if(
          (data.team === 0 && players[0].length < TEAM_SIZE) ||
          (data.team === 1 && players[1].length < TEAM_SIZE)
        ) {
          // TODO: Indentifying by socket: good Idea when disconnecting?
          // Push to player-array
          players[data.team].push({
            id: socket.id,
            name: data.name,
            position: {x: 0, y: 0}
          });
          // Join-status set to 1
          isjoined = true;
        }
        // If teams not valid or full
        else {
          socket.emit("rejected", {msg: "Team invalid or full."});
          // Initialize again (see above)
          socket.emit("initialize", {
            colors: TEAM_COLORS,
            fullteam: [
              players[0].length >= TEAM_SIZE,
              players[1].length >= TEAM_SIZE
            ]
          });
        }

        // If joining is accepted
        if (isjoined) {
          // Send joining-feedback
          socket.emit("joined", "Joined!");

          // If both teams even & full start readycheck!
          if(players[0].length + players[1].length === TEAM_SIZE*2) {
            ttg.emit("update-game-event", {id: "ready-check", msg: "Are you ready?"});
          }
        }
      });

      // Getting ready-messages from players
      socket.on("ready", function() {
        // Count'em up, check if everyone is ready
        playersready++;
        if(playersready === TEAM_SIZE*2) {

          // Reset gamefield
          for(let i = 0; i < 3; i++){
            for(let j = 0; j < 3; j++){
              beacon_status[i][j] = {
                players: [0, 0],
                owningTeam: 2,
                contestingTeam: 2,
                progress: 0
              };
            }
          }

          // Starting the countdown
          gamestatus = GAME_COUNTDOWN;
        }
      });

      // On position-update
      socket.on("update-nearest-beacon", function(beaconId) {

        // Check for sending player
        for(let i = 0; i < 2; i++) {
          for(let j = 0; j < TEAM_SIZE; j++) {
            // Prevent an error when reading socket.id
            try {
              // Player found
              if(players[i][j].id === socket.id){
                // Remove from old position
                beacon_status[players[i][j].position.x][players[i][j].position.y].players[i]--;
                // Save new position
                players[i][j].position = BEACON_LIST[beaconId];
                // Add to new position
                beacon_status[players[i][j].position.x][players[i][j].position.y].players[i]++;

                // Send current field status to current player
                socket.emit("update-field-status", {
                  fieldName: BEACON_NAMES[beaconId],
                  fieldColor: TEAM_COLORS[beacon_status[players[i][j].position.x][players[i][j].position.y].contestingTeam],
                  progress: beacon_status[players[i][j].position.x][players[i][j].position.y].progress
                });
              }
            } catch (e) {
              // TODO: ERROR
            }
          }
        }
      });

    });

    // Main periodic Game-Function
    function gameUpdate(){

      // Game starting
      // Countdown
      if(gamestatus > 0) {
        ttg.emit("update-game-event", {id: "game-countdown", msg: gamestatus});
        gamestatus = gamestatus - GAME_INTERVAL/1000;
        // If countdown reaches 0 send start to clients!
        if(gamestatus <= 0) {
          ttg.emit("update-game-event", {id: "game-start", msg: ""});
          gamestatus = 0;
        }
      }

      // Game running
      if(gamestatus === 0) {

        // Check if fields are in capturing process
        for(let i = 0; i < 3; i++){
          for(let j = 0; j < 3; j++){
            // Difference between teams
            var teamdiff = beacon_status[i][j].players[0] - beacon_status[i][j].players[1];

            // If team 0 is stronger
            if(teamdiff > 0) {
              // If team 0 owns field
              if(beacon_status[i][j].owningTeam === 0) {
                // If team 0 is not contesting team
                if (beacon_status[i][j].contestingTeam != 0) {
                  // Set contesting team to 0, reset progress to 1 - team 1s progress
                  beacon_status[i][j].contestingTeam = 0;
                  beacon_status[i][j].progress = 100 - beacon_status[i][j].progress;
                }
                // If progress is below 100
                if(beacon_status[i][j].progress < 100) {
                  // Get that progress up again!
                  beacon_status[i][j].progress += (teamdiff * (GAME_INTERVAL/(1000*GAME_CONQUERTIME))) * 100;
                  // If progress > 100, set to 100
                  if(beacon_status[i][j].progress > 100) {
                    beacon_status[i][j].progress = 100;
                  }
                }
              }
              // If team 0 does not own the field
              else {
                // If team 1 was contesting
                if (beacon_status[i][j].contestingTeam === 1) {
                  // Team 0 contests, starting at 100 - team 1s progress
                  beacon_status[i][j].contestingTeam = 0;
                    // If team 1 is the owner, start at 100-progress, else 0
                  if(beacon_status[i][j].owningTeam === 1)
                    beacon_status[i][j].progress = 100 - beacon_status[i][j].progress;
                  else {
                    beacon_status[i][j].progress = 0;
                  }
                }
                // Conquering!
                beacon_status[i][j].progress += (teamdiff * (GAME_INTERVAL/(1000*GAME_CONQUERTIME))) * 100;
                // If progress >= 100
                if (beacon_status[i][j].progress > 100) {
                  // 100 is maximum
                  beacon_status[i][j].progress = 100;
                  // If team 1 was the owning team before
                  if(beacon_status[i][j].owningTeam === 1) {
                    // Field is now neutral, progress at 0 again
                    beacon_status[i][j].owningTeam = 2;
                    beacon_status[i][j].progress = 0;
                  }
                  else {
                    // Field is now team 0s property!
                    beacon_status[i][j].owningTeam = 0;
                  }
                }
              }
            }

            // If team 1 is stronger
            else if(teamdiff < 0) {
              teamdiff = teamdiff * -1;
                // If team 1 owns field
              if(beacon_status[i][j].owningTeam === 1) {
                  // If team 1 is not contesting team
                if (beacon_status[i][j].contestingTeam != 1) {
                    // Set contesting team to 1, reset progress to 1 - team 1s progress
                  beacon_status[i][j].contestingTeam = 1;
                  beacon_status[i][j].progress = 100 - beacon_status[i][j].progress;
                }
                  // If progress is below 100
                if(beacon_status[i][j].progress < 100) {
                    // Get that progress up again!
                  beacon_status[i][j].progress += (teamdiff * (GAME_INTERVAL/(1000*GAME_CONQUERTIME))) * 100;
                    // If progress > 100, set to 100
                  if(beacon_status[i][j].progress > 100) {
                    beacon_status[i][j].progress = 100;
                  }
                }
              }
                // If team 1 does not own the field
              else {
                // If team 0 was contesting
                if (beacon_status[i][j].contestingTeam === 0) {
                  // Team 1 contests, starting at 100 - team 1s progress
                  beacon_status[i][j].contestingTeam = 1;
                  // If team 0 is the owner, start at 100-progress, else 0
                  if(beacon_status[i][j].owningTeam === 0)
                    beacon_status[i][j].progress = 100 - beacon_status[i][j].progress;
                  else {
                    beacon_status[i][j].progress = 0;
                  }
                }
                  // Conquering!
                beacon_status[i][j].progress += (teamdiff * (GAME_INTERVAL/(1000*GAME_CONQUERTIME))) * 100;
                  // If progress >= 100
                if (beacon_status[i][j].progress > 100) {
                    // 100 is maximum
                  beacon_status[i][j].progress = 100;
                    // If team 0 was the owning team before
                  if(beacon_status[i][j].owningTeam === 0) {
                      // Field is now neutral, progress at 0 again
                    beacon_status[i][j].owningTeam = 2;
                    beacon_status[i][j].progress = 0;
                  }
                  else {
                      // Field is now team 1s property!
                    beacon_status[i][j].owningTeam = 1;
                  }
                }
              }
            }
            else {
              // No contesting, nothing to do?
            }
            // Update current field for output-client
            ttg.emit("update-led", {id: FIELD_NUM[i][j], color: LED_COLORS[beacon_status[i][j].owningTeam]});
          }
        }

        // Does a team owns a row?
        for(let i = 0; i < 3; i++) {
          if (
            (beacon_status[i][0].owningTeam === beacon_status[i][1].owningTeam) &&
            (beacon_status[i][1].owningTeam === beacon_status[i][2].owningTeam)
          ) {
            if(beacon_status[i][0].owningTeam != 2){
              iswinner[beacon_status[i][0].owningTeam] = true;
            }
          }
        }

        // Does a team owns a column?
        for(let j = 0; j < 3; j++) {
          if (
            (beacon_status[0][j].owningTeam === beacon_status[1][j].owningTeam) &&
            (beacon_status[1][j].owningTeam === beacon_status[2][j].owningTeam)
          ) {
            if(beacon_status[0][j].owningTeam != 2){
              iswinner[beacon_status[0][j].owningTeam] = true;
            }
          }
        }

        // Does a team owns a diagonal?
        if (
          (
            (beacon_status[0][0].owningTeam === beacon_status[1][1].owningTeam) &&
            (beacon_status[1][1].owningTeam === beacon_status[2][2].owningTeam)
          ) ||
          (
            (beacon_status[0][2].owningTeam === beacon_status[1][1].owningTeam) &&
            (beacon_status[1][1].owningTeam === beacon_status[2][0].owningTeam)
          )
      ) {
          if(beacon_status[1][1].owningTeam != 2){
            iswinner[beacon_status[1][1].owningTeam] = true;
          }
        }

        // If there is a winner
        if (iswinner[0] || iswinner[1]) {
          // Team 0 wins!
          if(iswinner[0]) {
            ttg.emit("update-game-event", {id: "game-over", msg: "Team A wins!"});
            // Team 1 wins!
          } else if (iswinner[1]) {
            ttg.emit("update-game-event", {id: "game-over", msg: "Team B wins!"});
            // The rare draw appears!
          } else {
            ttg.emit("update-game-event", {id: "game-over", msg: "Draw!"});
          }

          // Reset game status
          gamestatus = -1;
          // Reset winners
          iswinner = [false, false];
          // Reset players
          players = [[], []];
          // Reset ready-check
          playersready = 0;

          // Reset gamefield
          for(let i = 0; i < 3; i++){
            for(let j = 0; j < 3; j++){
              beacon_status[i][j] = {
                players: [0, 0],
                owningTeam: 2,
                contestingTeam: 2,
                progress: 0
              };
            }
          }

        }

      }

      // No game if gamestatus < 0

      setTimeout(gameUpdate, GAME_INTERVAL);
    }

    gameUpdate();
  },
  // Return authors
  getAuthors() {
    return ["Yeganeh Pashai","Finn Schröder", "Felix Sjöö" ];
  },
  // Return description
  getInformation() {
    return "TicTacToe, Live, 5-gegen-5.";
  },
  // Return title
  getTitle() {
    return "TicTacGo";
  }
};
