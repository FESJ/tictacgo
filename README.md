# TicTacGo

_ein Augmented-Reality-Game für die Jahresausstellung 2016_

## Projektteilnehmer

Yeganeh Pashai _(1928048)_, Finn Schröder _(2116496)_, Felix Sjöö _(2250695)_

## Konzept
* [altes Konzept (ARTron)](/doc/ARTron_old/MoSy_ARTron_Konzept.pdf)
* [aktuelles Konzept (Tic, Tac, Go!)](/doc/TicTacGo/TicTacGo-Konzept.pdf)

## Dokumentation
* [Technische Dokumentation](/doc/TicTacGo/TicTacGo-Dokumentation.pdf)

## Präsentation
* [Plakat](/doc/TicTacGo/Plakat/TicTacGo-Plakat.pdf)

## Quellcode
* [Node.js-Server](/src/server/)
* [Cordova-App](/src/app/)
* [Python-DMX-Steuerung](/src/output/)